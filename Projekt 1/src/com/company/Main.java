package com.company;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {

        Scanner keyboard = new Scanner(System.in);
        String input = keyboard.nextLine();


        // Aufgaben a bis d

        if (input.contains("@") && input.contains(".")) {
            System.out.println("Email eingelesen");
        }
        if ((input.contains(".") || input.contains("-")) && input.length() > 8) {
            System.out.println("Datum eingelesen");
        }
        if (input.contains(":") && input.length() <= 5) {
            System.out.println("Urzeit eingelesen");
        }
        if (input.contains(".") && input.length() == 12) {
            System.out.println("IP-Adresse eingelesen");
        }
    }
}
